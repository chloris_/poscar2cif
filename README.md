# poscar2cif

A simple script to convert a structure in VASP POSCAR format to CIF with space group P1 (lowest possible symmetry).

The purpose of the script is to allow the user to view the resulting structure of [VASP](https://www.vasp.at/) calculation in a program specialized for viewing CIF structures (like [Mercury](https://www.ccdc.cam.ac.uk/solutions/csd-core/components/mercury/)).


## Requirements

The script requires only [Python 3](https://www.python.org/) interpreter.


## Usage

Running the script without arguments will print usage information:

```console
python poscar2cif.py
```

The following example will convert the result of VASP calcualtion in file `CONTCAR` to a CIF file `cif_file.cif`:

```console
python poscar2cif.py -o cif_file.cif CONTCAR
```

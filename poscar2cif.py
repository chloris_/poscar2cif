#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 17 07:40:42 2018

@author: chloris
"""


import math


class Poscar:
    
    class Stage:
        SPECIES_ORDER = 0
        UNIT = 1
        CELL_VECTORS = 2
        SPECIES_NUMBERS = 3
        DIRECT_KEYWORD = 4
        ATOM_POSIITONS = 5
    
    SPECIES_ORDER_KEYWORD = "Species order:"
    DIRECT_KEYWORD = "Direct"
    
    
    def __init__(self):
        self.__unit = None # Å
        self.__vector_a = []
        self.__vector_b = []
        self.__vector_c = []
        self.species_names = []
        self.species_numbers = []
        self.atom_positions = []
    
    
    def read_file(self, path):
        line_counter = 0
        cell_vector_counter = 0
        atom_positions_counter = 0
        n_atom_positions = None
        stage = self.Stage.SPECIES_ORDER
        contcar_format = False
        
        try:
            with open(path, "r") as poscar:
                for line in poscar:
                    line_counter += 1
                    
                    if stage == self.Stage.SPECIES_ORDER:
                        if contcar_format:
                            split = line.split(" ")
                            for s in split:
                                s = s.strip()
                                if len(s) > 0:
                                    self.species_names.append(s)
                            if len(self.species_names) == 0:
                                print("No species found when reading POSCAR!")
                                return False
                            else:
                                print("Species order found: " +
                                      str(self.species_names)) # DEBUG
                                stage = self.Stage.SPECIES_NUMBERS
                        else:
                            i = line.rfind(self.SPECIES_ORDER_KEYWORD)
                            if i < 0:
                                print("No species order found when reading "
                                      "POSCAR! Using CONTCAR format.")
                                contcar_format = True
                                stage = self.Stage.UNIT
                            else:
                                order = line[i + len(self.SPECIES_ORDER_KEYWORD):]
                                split = order.split(" ")
                                for s in split:
                                    s = s.strip()
                                    if len(s) > 0:
                                        self.species_names.append(s)
                                if len(self.species_names) == 0:
                                    print("No species found when reading POSCAR!")
                                    return False
                                else:
                                    print("Species order found: " +
                                          str(self.species_names)) # DEBUG
                                    stage = self.Stage.UNIT
                    
                    elif stage == self.Stage.UNIT:
                        try:
                            f = float(line.strip())
                            self.__unit = f
                            stage = self.Stage.CELL_VECTORS
                            print("Unit found: {0}".format(self.__unit))
                        except ValueError:
                            print("Skipping line {0}".format(line_counter))
                            continue
                    
                    elif stage == self.Stage.CELL_VECTORS:
                        split = line.split(" ")
                        components = []
                        for s in split:
                            s = s.strip()
                            if len(s) > 0:
                                try:
                                    if len(components) >= 3:
                                        print("Too many parameters found when "
                                              "parsing cell vectors in line "
                                              "{0}, skipping"
                                              .format(line_counter))
                                        break
                                    f = float(s)
                                    components.append(f)
                                except ValueError:
                                    print("Error when parsing cell vectors in "
                                          "line {0}".format(line_counter))
                                    continue
                        if len(components) == 3:
                            cell_vector_counter += 1
                            print("Cell vector {0} found: {1}"
                                  .format(cell_vector_counter,
                                          str(components))) # DEBUG
                            if cell_vector_counter == 1:
                                self.__vector_a = components
                            elif cell_vector_counter == 2:
                                self.__vector_b = components
                            elif cell_vector_counter == 3:
                                self.__vector_c = components
                                if contcar_format:
                                    stage = self.Stage.SPECIES_ORDER
                                else:
                                    stage = self.Stage.SPECIES_NUMBERS
                            else:
                                print("Invalid cell vector counter {0}!"
                                      .format(cell_vector_counter))
                    
                    elif stage == self.Stage.SPECIES_NUMBERS:
                        split = line.split(" ")
                        numbers = []
                        for s in split:
                            s = s.strip()
                            if len(s) != 0:
                                try:
                                    f = int(s)
                                    numbers.append(f)
                                except ValueError:
                                    print("Could not convert '{0}' to number "
                                          "while looking for numbers of "
                                          "species in line {1}!"
                                          .format(s, line_counter))
                                    return False
                        if len(numbers) == len(self.species_names):
                            for n in numbers:
                                self.species_numbers.append(n)
                            stage = self.Stage.DIRECT_KEYWORD
                            # Calculate number of atom positions to read later
                            n_atom_positions = 0
                            for n in self.species_numbers:
                                n_atom_positions += n
                            # DEBUG
                            print("Species numbers found: {0}"
                                  .format(self.species_numbers))
                        else:
                            print("Wrong number of species numbers in "
                                  "line {0} (does not match number of "
                                  "species)!".format(line_counter))
                            return False
                    
                    elif stage == self.Stage.DIRECT_KEYWORD:
                        if line.strip() == self.DIRECT_KEYWORD:
                            stage = self.Stage.ATOM_POSIITONS
                            print("Direct found!") # DEBUG
                        elif line.strip() != "":
                            print("Missing keyword '{0}' in line {1}!"
                                  .format(self.DIRECT_KEYWORD, line_counter))
                            return False
                    
                    elif stage == self.Stage.ATOM_POSIITONS:
                        if atom_positions_counter >= n_atom_positions:
                            print("Read required number of atom positions, "
                                  "skipping the rest of the file")
                            break
                        split = line.split()
                        numbers = []
                        for s in split:
                            s = s.strip()
                            if len(s) > 0:
                                try:
                                    if len(numbers) >= 3:
                                        print("Too many parameters found when "
                                              "parsing atom positions in line "
                                              "{0}, skipping"
                                              .format(line_counter))
                                        break
                                    f = float(s)
                                    numbers.append(f)
                                except ValueError:
                                    print("Could not convert '{0}' to number "
                                          "while reading atom positions in "
                                          "line {1}!".format(s, line_counter))
                        if len(numbers) == 3:
                            atom_positions_counter += 1
                            self.atom_positions.append(numbers)
                            print("Atom position: {0}".format(numbers))
                        else:
                            print("Skipping line {0}".format(line_counter))
                    
                    else:
                        print("Invalid stage when reading POSCAR file!")
                        return False
        
        except IOError as e:
            print("Error when reading POSCAR file:")
            print(e.strerror())
            return False
        
        return True
    
    
    def __calculate_vector_length(self, vector):
        length = 0
        for component in vector:
            length += component ** 2
        length = length ** 0.5
        return length * self.__unit
    
    
    def get_vector_a_length(self):
        return self.__calculate_vector_length(self.__vector_a)
    
    def get_vector_b_length(self):
        return self.__calculate_vector_length(self.__vector_b)
    
    def get_vector_c_length(self):
        return self.__calculate_vector_length(self.__vector_c)
    
    
    def __calculate_cos_phi(self, vector1, vector2):
        scalar_product = 0.0
        for i in range(len(vector1)):
            scalar_product += vector1[i] * vector2[i]
        scalar_product *= self.__unit ** 2.0
        vector1_length = self.__calculate_vector_length(vector1)
        vector2_length = self.__calculate_vector_length(vector2)
        cos_phi = scalar_product / (vector1_length * vector2_length)
        return cos_phi
    
    
    def __calculate_angle(self, vector1, vector2):
        """
          Calculates angle between given vectors in radians.
        """
        cos_phi = self.__calculate_cos_phi(vector1, vector2)
        return math.acos(cos_phi)
    
    
    def get_alpha(self):
        angle = self.__calculate_angle(self.__vector_b, self.__vector_c)
        return math.degrees(angle)
    
    def get_beta(self):
        angle = self.__calculate_angle(self.__vector_a, self.__vector_c)
        return math.degrees(angle)
    
    def get_gamma(self):
        angle = self.__calculate_angle(self.__vector_a, self.__vector_b)
        return math.degrees(angle)
    
    
    def get_volume(self):
        volume = self.get_vector_a_length() * \
                 self.get_vector_b_length() * \
                 self.get_vector_c_length()
        cos_alpha = self.__calculate_cos_phi(self.__vector_b, self.__vector_c)
        cos_beta = self.__calculate_cos_phi(self.__vector_a, self.__vector_c)
        cos_gamma = self.__calculate_cos_phi(self.__vector_a, self.__vector_b)
        factor = (1 - cos_alpha ** 2.0 - cos_beta ** 2.0 - cos_gamma ** 2.0 + \
                 2.0 * cos_alpha * cos_beta * cos_gamma) ** 0.5
        volume *= factor
        return volume
    
    
    def normalize_atom_positions(self):
        for i in range(len(self.atom_positions)):
          for j in range(len(self.atom_positions[i])):
            while self.atom_positions[i][j] < 0.0:
              self.atom_positions[i][j] += 1.0
            while self.atom_positions[i][j] > 1.0:
              self.atom_positions[i][j] -= 1.0
    
    
class Cif:
    
    
    CELL_FORMAT_STRING = "{0:20} {1:10f}\n"
    ATOM_POSITION_FORMAT_STRING = "{0:5} {1:5} {2:10.6f} {3:10.6f} {4:10.6f}\n"
    
    
    def __init__(self, structure_name = "Structure"):
        self.__structure_name = structure_name
    
    
    def poscar_to_cif(self, poscar, write_path):
        atoms_sum = 0
        for num in poscar.species_numbers:
            atoms_sum += num
        if atoms_sum != len(poscar.atom_positions):
            print("Number of atoms and atom positions are not equal!")
            return False
        
        try:
            with open(write_path, "w", newline = "\r\n") as cif_file:
                cif_file.write("data_{0}\n\n".format(self.__structure_name))
                cif_file.write("_symmetry_space_group_name_H-M 'P 1'\n\n")
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_length_a",
                        poscar.get_vector_a_length()))
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_length_b",
                        poscar.get_vector_b_length()))
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_length_c",
                        poscar.get_vector_c_length()))
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_angle_alpha",
                        poscar.get_alpha()))
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_angle_beta",
                        poscar.get_beta()))
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_angle_gamma",
                        poscar.get_gamma()))
                cif_file.write(self.CELL_FORMAT_STRING.format(
                        "_cell_volume",
                        poscar.get_volume()))
                cif_file.write("loop_\n")
                cif_file.write("   _atom_site_label\n")
                cif_file.write("   _atom_site_type_symbol\n")
                cif_file.write("   _atom_site_fract_x\n")
                cif_file.write("   _atom_site_fract_y\n")
                cif_file.write("   _atom_site_fract_z\n")
                
                # Write atom positions
                atom_index = 0
                
                for species_index in range(len(poscar.species_names)):
                    for number in range(poscar.species_numbers[species_index]):
                        atom_symbol = poscar.species_names[species_index]
                        atom_label = atom_symbol + str(number + 1)
                        cif_file.write(
                            self.ATOM_POSITION_FORMAT_STRING
                            .format(atom_label, atom_symbol,
                                    poscar.atom_positions[atom_index][0],
                                    poscar.atom_positions[atom_index][1],
                                    poscar.atom_positions[atom_index][2]))
                        atom_index += 1
            
        except IOError as e:
            print ("Error while writing CIF file:")
            print(e.strerror())
            return False
        
        return True


def print_help():
    print(
"""
Usage: python poscar2cif.py -t <title> [-o <output file>] <input file>
Example: python poscar2cif.py -t Structure POSCAR

--help: print this message
-o or --output: path to output CIF file (.cif extension is added automatically
                if missing)
-t or --title: title of the structure, also used as file name if output path
               is missing (.cif extension added automatically)
-w or --overwrite: write CIF file even if it already exists
""")


# --------------------------------------------------------------------
#    Main program
# --------------------------------------------------------------------
        
import getopt
import sys
import os.path

# Parse command line args
if len(sys.argv) < 2:
    print_help()
    sys.exit()

SHORT_OPTS = "t:o:w"
LONG_OPTS = ["help", "title=", "output=", "overwrite"]

title = "Structure"
output_path = None
input_path = None
overwrite = False

try:
    opts, trailing_args = getopt.getopt(
        sys.argv[1:], SHORT_OPTS, LONG_OPTS)
except getopt.GetoptError:
    print("Error while parsing command line arguments!")
    sys.exit()

for option, value in opts:
    if option == "--help":
        print_help()
        sys.exit()
    elif option == "-o" or option == "--output":
        output_path = value
        if not output_path.lower().endswith(".cif"):
            output_path += ".cif"
    elif option == "-t" or option == "--title":
        if value == "":
            print("Title must not be an empty string!")
            sys.exit()
        title = value.strip()
    elif option == "-w" or option == "--overwrite":
        overwrite = True

# Input file
if len(trailing_args) == 0:
    print("Missing path to POSCAR file!")
    sys.exit()
elif len(trailing_args) > 1:
    print("Converting multiple files at once is not supported!")
    sys.exit()

input_path = trailing_args[0]
if not os.path.isfile(input_path):
    print("File '{0}' does not exist!".format(input_path))
    sys.exit()

# Generate output path if not provided
if output_path == None:
    output_path = title
    if not output_path.lower().endswith(".cif"):
        output_path += ".cif"
# Check if output file already exists
if os.path.isdir(output_path):
    print("'{0}' is a directory!".format(output_path))
if os.path.isfile(output_path):
    if overwrite:
        print("File '{0}' will be overwritten".format(output_path))
    else:
        print("File or directory '{0}' already exists!".format(output_path))
        print("Use overwrite option or pick another output path")
        sys.exit()
    

print("Reading POSCAR file '{0}'".format(input_path))
poscar = Poscar()
result = poscar.read_file(input_path)

if result:
    print("File '{0}' read".format(input_path))
    print("Normalizing atom positions")
    poscar.normalize_atom_positions()
else:
    print("Error while reading file '{0}'".format(input_path))
    sys.exit()

print("Writing CIF file '{0}'".format(output_path))
cif = Cif(title)
result = cif.poscar_to_cif(poscar, output_path)

if result:
    print("File '{0}' written".format(output_path))
else:
    print("Error while writing file '{0}'".format(output_path))